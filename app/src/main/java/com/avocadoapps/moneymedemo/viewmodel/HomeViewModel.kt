package com.avocadoapps.moneymedemo.viewmodel

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.avocadoapps.moneymedemo.data.AccountRepository
import com.avocadoapps.moneymedemo.data.model.LoanData
import java.text.NumberFormat

class HomeViewModel(
    private val accountRepository: AccountRepository
): ViewModel() {

    private val loanAmount = MutableLiveData<Int>(DEFAULT_AMOUNT)
    private val loanAmountLabel = MediatorLiveData<String>()
    private val loanDuration = MutableLiveData<Int>(DEFAULT_DURATION)
    private val loanDurationLabel = MediatorLiveData<String>()
    private val loanData = MutableLiveData<LoanData>()

    init {
        loanAmountLabel.addSource(loanAmount) {
            loanAmountLabel.value = "$${NumberFormat.getInstance().format(it)}"
        }
        loanDurationLabel.addSource(loanDuration) {
            loanDurationLabel.value = "$it months"
        }
    }

    fun getAmountLabel(): LiveData<String> = loanAmountLabel
    fun getDurationLabel(): LiveData<String> = loanDurationLabel
    fun getLoanData(): LiveData<LoanData> = loanData

    fun amountChanged(amount: Int) {
        loanAmount.value = amount * AMOUNT_INCREMENT
    }

    fun durationChanged(duration: Int) {
        loanDuration.value = duration
    }

    @SuppressLint("CheckResult")
    fun generateLoanData(identifier: String = "") {
        accountRepository.fetchAccount(identifier)
            .subscribe {
                val dataPresent = it.isNotEmpty()
                val data = LoanData(
                    loanAmount.value!!,
                    loanAmountLabel.value,
                    loanDuration.value!!,
                    loanDurationLabel.value,
                    null,
                    if(dataPresent) it.first().title else null,
                    if(dataPresent) it.first().firstName else null,
                    if(dataPresent) it.first().lastName else null,
                    if(dataPresent) it.first().birthDate else null,
                    null,
                    null
                )
                loanData.value = data
            }
    }

    companion object {
        const val AMOUNT_INCREMENT = 100
        const val DEFAULT_AMOUNT = 8500
        const val DEFAULT_DURATION = 20
    }

}