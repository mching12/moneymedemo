package com.avocadoapps.moneymedemo.data.model

sealed class LoanState {
    object InProgress: LoanState()
    object MissingFields: LoanState()
    object Success: LoanState()
    data class Error(val error: String?): LoanState()
}