package com.avocadoapps.moneymedemo.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import com.avocadoapps.moneymedemo.R
import com.avocadoapps.moneymedemo.data.model.LoanData
import com.avocadoapps.moneymedemo.databinding.ActivityLoanDetailsBinding
import com.avocadoapps.moneymedemo.viewmodel.LoanDetailsViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import com.avocadoapps.moneymedemo.data.model.LoanState
import kotlinx.android.synthetic.main.activity_loan_details.*
import java.text.NumberFormat


class LoanDetailsActivity : BaseActivity(), View.OnClickListener {

    private val viewModel: LoanDetailsViewModel by viewModel()
    private val durationList = ArrayList<String>()
    private val amountList = ArrayList<String>()
    private lateinit var binding: ActivityLoanDetailsBinding

    private val amountListener = object: AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(p0: AdapterView<*>?) {}
        override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
            viewModel.setNewAmount(amountList[spnAmount.selectedItemPosition])
        }
    }

    private val durationListener = object: AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(p0: AdapterView<*>?) {}
        override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
            viewModel.setNewDuration(durationList[spnDuration.selectedItemPosition])
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView<ActivityLoanDetailsBinding>(
            this, R.layout.activity_loan_details).apply {
            loanDetailsViewModel = viewModel
            lifecycleOwner = this@LoanDetailsActivity
        }
        setupInterface()
        setupViewModel()
    }

    private fun setupInterface() {
        setDurationData()
        setAmountData()
        spnDuration.onItemSelectedListener = durationListener
        spnAmount.onItemSelectedListener = amountListener
    }

    private fun setupViewModel() {
        val loanData = intent.getParcelableExtra<LoanData>(EXTRA_DATA)
        spnAmount.setSelection(amountList.indexOf(loanData?.amountDisplay))
        spnDuration.setSelection(durationList.indexOf(loanData?.durationDisplay))
        viewModel.apply {
            getMonthlyRepayment().observe(this@LoanDetailsActivity, Observer {
                tvRepayments.text = it
            })
            getLoanState().observe(this@LoanDetailsActivity, Observer {
                renderLoanState(it)
            })
            setPrefillData(loanData)
        }
    }

    private fun setDurationData() {
        for (i in DURATION_MIN..DURATION_MAX) {
            durationList.add("$i months")
        }
        spnDuration.adapter = ArrayAdapter(this, R.layout.layout_spinner, durationList)
    }

    private fun setAmountData() {
        for (i in AMOUNT_MIN..AMOUNT_MAX) {
            amountList.add("$${NumberFormat.getInstance().format(i* AMOUNT_INCREMENT)}")
        }
        spnAmount.adapter = ArrayAdapter(this, R.layout.layout_spinner, amountList)
    }

    private fun launchSuccessPage() {
        SuccessActivity.launch(this)
        setResult(Activity.RESULT_OK)
        finish()
    }

    private fun renderLoanState(loanState: LoanState) {
        if(loanState is LoanState.InProgress) showLoading()
        else hideDialog()
        when(loanState) {
            is LoanState.Error -> showToast(loanState.error.toString())
            is LoanState.Success -> launchSuccessPage()
            is LoanState.MissingFields ->
                showSnackbar(parentView, getString(R.string.error_missing_fields))
        }
    }

    override fun onClick(p0: View?) {
        if(p0?.id == btnProcess.id) viewModel.processLoan()
    }

    companion object {

        private const val EXTRA_DATA = "extra_data"
        private const val DURATION_MIN = 3
        private const val DURATION_MAX = 36
        private const val AMOUNT_MIN = 21
        private const val AMOUNT_MAX = 150
        private const val AMOUNT_INCREMENT = 100

        fun launch(activity: Activity, data: LoanData, requestCode: Int) {
            val intent = Intent(activity, LoanDetailsActivity::class.java).apply {
                putExtra(EXTRA_DATA, data)
            }
            activity.startActivityForResult(intent, requestCode)
        }
    }
}