package com.avocadoapps.moneymedemo.data.model

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "loan_data")
data class LoanData(
    @ColumnInfo(name = "amount")
    var amount: Int = 0,
    @ColumnInfo(name = "amount_display")
    var amountDisplay: String? = null,
    @ColumnInfo(name = "duration")
    var duration: Int = 0,
    @ColumnInfo(name = "duration_display")
    var durationDisplay: String? = null,
    @ColumnInfo(name = "reasons")
    var reasons: String? = null,
    @ColumnInfo(name = "title")
    var title: String? = null,
    @ColumnInfo(name = "first_name")
    var firstName: String? = null,
    @ColumnInfo(name = "last_name")
    var lastName: String? = null,
    @ColumnInfo(name = "birth_date")
    var birthDate: String? = null,
    @ColumnInfo(name = "email")
    var email: String? = null,
    @ColumnInfo(name = "mobile")
    var mobile: String? = null
): Parcelable {
    @IgnoredOnParcel
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}