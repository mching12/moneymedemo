package com.avocadoapps.moneymedemo.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.avocadoapps.moneymedemo.data.model.AccountData

@Dao
interface AccountDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAccount(account: AccountData)

    @Query("SELECT * FROM account_data WHERE email = :email AND password = :password")
    fun fetchAccount(email: String?, password: String?): Array<AccountData>

    @Query("SELECT * FROM account_data WHERE email = :email")
    fun fetchAccount(email: String): Array<AccountData>

}
