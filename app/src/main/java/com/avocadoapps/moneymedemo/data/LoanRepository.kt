package com.avocadoapps.moneymedemo.data

import com.avocadoapps.moneymedemo.data.local.LoanDao
import com.avocadoapps.moneymedemo.data.model.LoanData
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.text.NumberFormat

class LoanRepository (
    private val loanDao: LoanDao
) {

    fun insertLoan(amount: Int, duration: Int, reasons: String, title: String,
                   fname: String, lname: String, bday: String, email: String,
                   mobile: String): Observable<Unit> {
        val loanData = LoanData(amount,
            "$${NumberFormat.getInstance().format(amount)}", duration,
            "$duration months", reasons, title, fname, lname, bday, email, mobile)
        return Observable
            .just(loanDao.insertLoan(loanData))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

}