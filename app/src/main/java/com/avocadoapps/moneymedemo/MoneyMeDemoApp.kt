package com.avocadoapps.moneymedemo

import android.app.Application
import com.avocadoapps.moneymedemo.di.databaseModule
import com.avocadoapps.moneymedemo.di.repositoryModule
import com.avocadoapps.moneymedemo.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MoneyMeDemoApp: Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@MoneyMeDemoApp)
            modules(listOf(viewModelModule, repositoryModule, databaseModule))
        }
    }
}