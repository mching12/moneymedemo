package com.avocadoapps.moneymedemo.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.avocadoapps.moneymedemo.data.model.AccountData
import com.avocadoapps.moneymedemo.data.model.LoanData

@Database(entities = [AccountData::class, LoanData::class], version = 1, exportSchema = false)
@TypeConverters
abstract class AppDatabase : RoomDatabase() {

    abstract fun accountDao(): AccountDao
    abstract fun loanDao(): LoanDao

    companion object {

        const val DATABASE_NAME = "account_database.db"

        @Volatile private var instance: AppDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context)= instance ?: synchronized(LOCK){
            instance ?: buildDatabase(context).also { instance = it}
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME)
                .allowMainThreadQueries()
                .build()
    }
}