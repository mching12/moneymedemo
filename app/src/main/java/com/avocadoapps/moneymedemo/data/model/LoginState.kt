package com.avocadoapps.moneymedemo.data.model

sealed class LoginState {
    object InProgress: LoginState()
    object Invalid: LoginState()
    data class Succes(val identifier: String): LoginState()
    data class Error(val error: String?): LoginState()
}