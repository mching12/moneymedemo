package com.avocadoapps.moneymedemo.data

import com.avocadoapps.moneymedemo.data.local.AccountDao
import com.avocadoapps.moneymedemo.data.model.AccountData
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AccountRepository (
    private val accountDao: AccountDao
) {

    fun fetchAccount(email: String?, password: String?): Observable<Array<AccountData>> {
        return Observable
            .just(accountDao.fetchAccount(email, password))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun fetchAccount(identifier: String): Observable<Array<AccountData>> {
        return Observable
            .just(accountDao.fetchAccount(identifier))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun addAccount(title: String, fname: String, lname: String, bday: String,
                   mobile: String, email: String): Observable<Unit> {
        return Observable
            .just(accountDao.insertAccount(createAccountData(title, fname,
                lname, bday, mobile, email)))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    private fun createAccountData(title: String, fname: String, lname: String, bday: String,
                                  mobile: String, email: String): AccountData {
        return  AccountData(title, fname, lname, bday, mobile, email, DEFAULT_PASSWORD)
    }

    companion object {
        private const val DEFAULT_PASSWORD = "password"
    }
}