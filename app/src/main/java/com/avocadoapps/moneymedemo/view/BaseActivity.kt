package com.avocadoapps.moneymedemo.view

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.avocadoapps.moneymedemo.R
import com.google.android.material.snackbar.Snackbar

abstract class BaseActivity : AppCompatActivity() {

    private var dialog: Dialog? = null

    protected fun showSnackbar(view: View, message: String) {
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show()
    }

    protected fun hideDialog() {
        dialog?.dismiss()
    }

    protected fun showLoading() {
        if(dialog == null) {
            val view = LayoutInflater.from(this).inflate(R.layout.dialog_layout, null)
            dialog = Dialog(this)
                .apply {
                    requestWindowFeature(Window.FEATURE_NO_TITLE)
                    setContentView(view)
                    window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                    setCanceledOnTouchOutside(false)
                    setCancelable(false)
                }
        }
        dialog?.show()
    }

    protected fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}