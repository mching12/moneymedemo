package com.avocadoapps.moneymedemo.data.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "account_data")
data class AccountData(
    @ColumnInfo(name = "title")
    var title: String? = null,
    @ColumnInfo(name = "first_name")
    var firstName: String? = null,
    @ColumnInfo(name = "last_name")
    var lastName: String? = null,
    @ColumnInfo(name = "birth_date")
    var birthDate: String? = null,
    @ColumnInfo(name = "mobile")
    var mobile: String? = null,
    @PrimaryKey @NonNull
    var email: String = "",
    @ColumnInfo(name = "password")
    var password: String? = null
)