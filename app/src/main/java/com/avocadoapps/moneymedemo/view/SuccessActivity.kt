package com.avocadoapps.moneymedemo.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.avocadoapps.moneymedemo.R
import kotlinx.android.synthetic.main.activity_success.*

class SuccessActivity : BaseActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_success)
        setupInterface()
    }

    private fun setupInterface() {
        btnHome.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        if(p0?.id == btnHome.id) goHome()
    }

    private fun goHome() {
        HomeActivity.launch(this)
        finish()
    }

    companion object {
        fun launch(activity: Activity) {
            activity.startActivity(Intent(activity, SuccessActivity::class.java))
        }
    }
}
