package com.avocadoapps.moneymedemo.viewmodel

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.avocadoapps.moneymedemo.data.AccountRepository
import com.avocadoapps.moneymedemo.data.LoanRepository
import com.avocadoapps.moneymedemo.data.model.LoanData
import com.avocadoapps.moneymedemo.data.model.LoanState
import java.text.NumberFormat
import kotlin.math.pow


class LoanDetailsViewModel(
    private val loanRepository: LoanRepository,
    private val accountRepository: AccountRepository
): ViewModel() {

    private val monthlyRepayment = MediatorLiveData<String>()
    private val _loanState = MutableLiveData<LoanState>()
    private val amount = MutableLiveData<Int>()
    private val duration = MutableLiveData<Int>()
    val reasons = MutableLiveData<String>()
    val title = MutableLiveData<String>()
    val fname = MutableLiveData<String>()
    val lname = MutableLiveData<String>()
    val bday = MutableLiveData<String>()
    val email = MutableLiveData<String>()
    val mobile = MutableLiveData<String>()

    init {
        monthlyRepayment.addSource(amount) {
            generateRepayment()
        }
        monthlyRepayment.addSource(duration) {
            generateRepayment()
        }
    }

    fun getMonthlyRepayment(): LiveData<String> = monthlyRepayment
    fun getLoanState(): LiveData<LoanState> = _loanState

    fun setNewAmount(amountLabel: String) {
        val format = NumberFormat.getCurrencyInstance()
        val number = format.parse(amountLabel)
        this.amount.value = number?.toInt()
    }

    fun setNewDuration(durationLabel: String) {
        this.duration.value = durationLabel.replace(" months","").toInt()
    }

    private fun computePmt(amount: Double, duration: Double, interest: Double): Double {
        val v = 1 + interest / MONTHS_PER_YEAR
        val t = - (duration / MONTHS_PER_YEAR) * MONTHS_PER_YEAR
        return amount * (interest / MONTHS_PER_YEAR) / (1 - v.pow(t))
    }

    private fun generateRepayment() {
        if(amount.value == null || duration.value == null) return
        monthlyRepayment.value = "$${NumberFormat.getInstance()
            .format(computePmt(amount.value!!.toDouble(), duration.value!!.toDouble(), annual_interest))}"
    }

    @SuppressLint("CheckResult")
    fun processLoan() {
        if(amount.value == null || duration.value == null || reasons.value.isNullOrEmpty() ||
            title.value.isNullOrEmpty() || fname.value.isNullOrEmpty() ||
            lname.value.isNullOrEmpty() || bday.value.isNullOrEmpty() ||
            email.value.isNullOrEmpty() || mobile.value.isNullOrEmpty()) {
            _loanState.value = LoanState.MissingFields
            return
        }

        accountRepository.addAccount(title = title.value!!, fname = fname.value!!,
            lname = lname.value!!, mobile = mobile.value!!, email = email.value!!, bday = bday.value!!)
            .doOnSubscribe { _loanState.value = LoanState.InProgress }
            .subscribe {
                loanRepository.insertLoan(amount = amount.value!!, duration = duration.value!!, bday = bday.value!!,
                    email = email.value!!, fname = fname.value!!, lname = lname.value!!, mobile = mobile.value!!,
                    reasons = reasons.value!!, title = title.value!!)
                    .subscribe ({
                        _loanState.value = LoanState.Success
                    }, {
                        _loanState.value = LoanState.Error(it.localizedMessage)
                    })
            }
    }

    fun setPrefillData(data: LoanData?) {
        if(data == null) return
        amount.value = data.amount
        duration.value = data.duration
        title.value = data.title
        fname.value = data.firstName
        lname.value = data.lastName
        bday.value = data.birthDate
    }

    companion object {
        private const val annual_interest = 0.0899
        private const val MONTHS_PER_YEAR = 12
    }
}