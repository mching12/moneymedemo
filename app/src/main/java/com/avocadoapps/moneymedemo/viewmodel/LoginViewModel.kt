package com.avocadoapps.moneymedemo.viewmodel

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.avocadoapps.moneymedemo.data.AccountRepository
import com.avocadoapps.moneymedemo.data.model.LoginState

class LoginViewModel(
    private val accountRepository: AccountRepository
): ViewModel() {

    private val _loginState = MutableLiveData<LoginState>()
    var email = MutableLiveData<String>()
    var password = MutableLiveData<String>()

    fun getLoginState(): LiveData<LoginState> = _loginState

    @SuppressLint("CheckResult")
    fun login() {
        if(email.value.isNullOrBlank() || password.value.isNullOrBlank()) return
        accountRepository.fetchAccount(email.value, password.value)
            .doOnSubscribe { _loginState.postValue(LoginState.InProgress) }
            .subscribe {
                if(it.isEmpty()) _loginState.value = LoginState.Invalid
                else _loginState.value = LoginState.Succes(it.first().email)
            }
    }
}