package com.avocadoapps.moneymedemo.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import com.avocadoapps.moneymedemo.data.model.LoanData

@Dao
interface LoanDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLoan(loanData: LoanData)

}