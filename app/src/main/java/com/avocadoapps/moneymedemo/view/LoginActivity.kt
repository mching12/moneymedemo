package com.avocadoapps.moneymedemo.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.avocadoapps.moneymedemo.R
import com.avocadoapps.moneymedemo.data.model.LoginState
import com.avocadoapps.moneymedemo.databinding.ActivityLoginBinding
import com.avocadoapps.moneymedemo.view.HomeActivity.Companion.EXTRA_DATA
import com.avocadoapps.moneymedemo.viewmodel.LoginViewModel
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginActivity : BaseActivity(), View.OnClickListener {

    private val viewModel: LoginViewModel by viewModel()
    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView<ActivityLoginBinding>(
            this, R.layout.activity_login).apply {
            loginViewModel = viewModel
            lifecycleOwner = this@LoginActivity
        }
        setupInterface()
        setupViewModel()
    }

    private fun setupInterface() {
        login.setOnClickListener(this)
        ivClose.setOnClickListener(this)
    }

    private fun setupViewModel() {
        viewModel.getLoginState().observe(this, Observer {
            renderLoginState(it)
        })
    }

    private fun renderLoginState(loginState: LoginState) {
        if(loginState is LoginState.InProgress) showLoading()
        else hideDialog()
        tvError.visibility = if(loginState is LoginState.Invalid)
            View.VISIBLE else View.INVISIBLE
        when(loginState) {
            is LoginState.Succes -> loginSuccess(loginState.identifier)
            is LoginState.Error -> showSnackbar(parentView, loginState.error.toString())
        }
    }

    private fun loginSuccess(identifier: String) {
        val returnData = Intent().apply {
            putExtra(EXTRA_DATA, identifier)
        }
        setResult(Activity.RESULT_OK, returnData)
        finish()
    }

    override fun onClick(p0: View?) {
        if(p0?.id == ivClose.id) {
            setResult(RESULT_CANCELED)
            finish()
        }
    }

    companion object {

        fun launch(activity: Activity, requestCode: Int) {
            val intent = Intent(activity, LoginActivity::class.java)
            activity.startActivityForResult(intent, requestCode)
        }
    }
}