package com.avocadoapps.moneymedemo.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.SeekBar
import android.widget.TextView
import androidx.lifecycle.Observer
import com.avocadoapps.moneymedemo.R
import com.avocadoapps.moneymedemo.data.model.LoanData
import com.avocadoapps.moneymedemo.viewmodel.HomeViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class HomeActivity : BaseActivity(), SeekBar.OnSeekBarChangeListener, View.OnClickListener {

    private val homeViewModel: HomeViewModel by viewModel()

    //  PR1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupInterface()
        setupViewModel()
    }

    private fun setupInterface() {
        sbAmount.setOnSeekBarChangeListener(this)
        sbDuration.setOnSeekBarChangeListener(this)
        btnLogin.setOnClickListener(this)
        btnRegister.setOnClickListener(this)
    }

    private fun setupViewModel() {
        homeViewModel.apply {
            getAmountLabel().observe(this@HomeActivity, Observer {
                tvAmount.text = it
            })
            getDurationLabel().observe(this@HomeActivity, Observer {
                tvDuration.text = it
            })
            getLoanData().observe(this@HomeActivity, Observer {
                launchLoanPage(it)
            })
        }
    }

    private fun moveTooltip(seekBar: SeekBar, tooltip: TextView) {
        tooltip.x = seekBar.x + seekBar.thumb.bounds.left
    }

    private fun amountChanged(amount: Int) {
        moveTooltip(sbAmount, tvAmount)
        homeViewModel.amountChanged(amount)
    }

    private fun durationChanged(duration: Int) {
        moveTooltip(sbDuration, tvDuration)
        homeViewModel.durationChanged(duration)
    }

    private fun launchLogin() {
        LoginActivity.launch(this, CODE_REQUEST_LOGIN)
    }

    private fun launchLoanPage(data: LoanData) {
        LoanDetailsActivity.launch(this, data, CODE_REQUEST_LOAN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == CODE_REQUEST_LOGIN && resultCode == Activity.RESULT_OK) {
            val identifier = data?.getStringExtra(EXTRA_DATA) ?: ""
            homeViewModel.generateLoanData(identifier)
        } else if(requestCode == CODE_REQUEST_LOAN && resultCode == Activity.RESULT_OK) {
            finish()
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onStartTrackingTouch(p0: SeekBar?) {}
    override fun onStopTrackingTouch(p0: SeekBar?) {}

    override fun onProgressChanged(p0: SeekBar?, progress: Int, p2: Boolean) {
        when(p0?.id) {
            sbAmount.id -> amountChanged(progress)
            sbDuration.id -> durationChanged(progress)
        }
    }

    override fun onClick(p0: View?) {
        when(p0?.id) {
            btnLogin.id -> launchLogin()
            btnRegister.id -> homeViewModel.generateLoanData()
        }
    }

    companion object {
        const val CODE_REQUEST_LOGIN = 1212
        const val CODE_REQUEST_LOAN = 1213
        const val EXTRA_DATA = "extra_id"

        fun launch(activity: Activity) {
            activity.startActivity(Intent(activity, HomeActivity::class.java))
        }
    }
}
