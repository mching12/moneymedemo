package com.avocadoapps.moneymedemo.di

import androidx.room.Room
import com.avocadoapps.moneymedemo.data.AccountRepository
import com.avocadoapps.moneymedemo.data.LoanRepository
import com.avocadoapps.moneymedemo.data.local.AppDatabase
import com.avocadoapps.moneymedemo.data.local.AppDatabase.Companion.DATABASE_NAME
import com.avocadoapps.moneymedemo.viewmodel.HomeViewModel
import com.avocadoapps.moneymedemo.viewmodel.LoanDetailsViewModel
import com.avocadoapps.moneymedemo.viewmodel.LoginViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val databaseModule = module {
    single {
        Room.databaseBuilder(get(), AppDatabase::class.java, DATABASE_NAME)
            .allowMainThreadQueries()
            .build()
    }
    single { get<AppDatabase>().accountDao() }
    single { get<AppDatabase>().loanDao() }
}

val repositoryModule = module {
    single { AccountRepository(get()) }
    single { LoanRepository(get()) }
}

val viewModelModule = module {
    viewModel { HomeViewModel(get()) }
    viewModel { LoginViewModel(get()) }
    viewModel { LoanDetailsViewModel(get(), get()) }
}
